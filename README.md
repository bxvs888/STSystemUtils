<h2>STSystemUtils</h2>

<pre><code>
// 拨打电话
+ (void)call:(NSString *)phoneNo;

// 发送短信
+ (MFMessageComposeViewController *)sendSMS:(NSString *)bodyOfMessage
recipientList:(NSArray *)recipients
delegate:(UIViewController<MFMessageComposeViewControllerDelegate> *)delegate;

// 是否联网
+ (BOOL)isNetConnect;

// 是否通过WiFi联网
+ (BOOL)isNetViaWiFi;

// 是否通过3G联网
+ (BOOL)isNetViaWWAN;

// 获取设备唯一码
+ (NSString *)guid;

// 系统版本
+ (NSString *)systemVersion;

// 屏幕尺寸
+ (CGSize)screenSize;

// 是否iPhone5
+ (BOOL)isIPhone5;

// 当前应用版本号
+ (NSString *)applicationVersion;

// Documents路径
+ (NSString *)documentsPath;

// 根据rgb生成颜色
+ (UIColor *)colorFromRGB:(NSInteger)rgbValue;

// 屏幕截图
+ (UIImage *)screenShotWithView:(UIView *)v; 
</code></pre>